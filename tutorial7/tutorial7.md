# Tutorial 7
The Manager hands you several new documents and some code. "Have you ever heard of the concept of Design Patterns?". Your memories of these Design Patterns start to surface in your mind. There seems to be quite a lot of Design Patterns, but for now you remember one of three types: **Behavioral Design Patterns**.

Behavioral Design Patterns identify the problems that appear with communication between objects. The patterns help realize these communications and increases flexibility when doing so. The two you remember are **Strategy Pattern** and **Observer Pattern**.

**NOTE : You might want to do Observer problem set before Strategy problem set**

## Strategy Pattern
As you try to remember what you know about Strategy Pattern, you ask some questions for yourself to answer: What is Strategy Pattern?

Strategy Pattern is a behavioral design pattern that allows program to choose algorithms at run time. You think that while this explanation is true, it doesn't really explain how Strategy Pattern works. Since the answer to the first question doesn't really help you, you ask another question : Why do we need Strategy Pattern?

Why do we need Strategy Pattern? Because Strategy Pattern can do what OOP inheritance can't do, and also other reasons related to maintainability. To demostrate why you need Strategy Pattern, you begin to think about examples. One example that you can think is about some problem with ducks in a certain book called "Head First Design Pattern". 

The ducks problem in the book starts with one problem : Behaviours between subclasses of Duck can be very different. Initially, a class named Duck has methods fly() and quack(), but there's some problem: not all ducks can fly and quack and not every duck fly and quack the same way with other ducks. One "solution" proposed is to make every subclass that can fly and quack to implement Flyable and Quackable. The "solution", while can eliminate mistakes from the last design, still has one problem : maintenance nightmare, every subclass will have to override the method fly() or quack() and there's some subclasses that share the same behaviour of fly() and quack() with other subclasses, which results to many duplicate code written.

So how the problem is solved? The problem is solved by using composition. We create interfaces named FlyBehaviour and QuackBehaviour that have method fly() and quack(), respectively. The Duck class will have one instance of FlyBehaviour and QuackBehaviour each. When the method fly() in Duck class is called, the FlyBehaviour instance will call the methods for the Duck class, that's also the case with quack(). With this way, not only we can reduce duplicate codes, we can flexibly define every subclass' behaviours and re-define said behaviours at run time. This is also an example of a principle called "composition over inheritance".

As you conclude your illustration, you think back about your questions that now you can answer. Why do we need Strategy Pattern? Strategy Pattern can do what OOP inheritance can't do: varying behaviour between subclasses. How does Strategy Pattern do that? Strategy Pattern do that by adhering to "composition over inheritance" principle.

Now that you understand Strategy Pattern, you examine the problem you have right now to see whether Strategy Pattern is suitable for this problem or not. As you examine it, you find that the problem have very similar nature with the duck problem, making Strategy Pattern very suitable for this problem.

## Strategy Pattern : The Task
Fortunately for you, the base code given to you is structured in a way that makes implementing Strategy Pattern very easy and The Association have your gratitude for that.

ou look at the code structure. The codes are structure as such:
```
controller
	StrategyController.java
core
	Action.java
	AttackAction.java (extends Action)
	DefenseAction.java (extends Action)
	SupportAction.java (extends Action)
	Magician.java
	AntiMagician.java (extends Magician)
	DeathMagician.java (extends Magician)
	Enhancer.java (extends Magician)
	Spellcaster.java (extends Magician)
	Harvest.java (implements AttackAction)
	MagicMissile.java (implements AttackAction)
	Barrier.java (implements DefenseAction)
	Counter.java (implements DefenseAction)
	Enhance.java (implements SupportAction)
	Regenerate.java (implements SupportAction)
repository
	ActionRepository.java
	MagicianRepository.java
service
	ActionService.java
	MagicianService.java
	ActionServiceImpl.java (implements ActionService)
	MagicianServiceImpl.java (implements MagicianService)
Tutorial7Application.java
```
You look thru every code file and find that there are some incomplete code. You make notes to yourself that there are some jobs to do. Those jobs are:
1. Complete the implementation of Magician.java so it can accommodate the implementation of Strategy Pattern
2. Complete the implementation of every Magician's subclass with given specification (read the code's "to do" l). 

You still think that the code design is not at best but you think it still do the work so it's another  for another time.

## Observer Pattern
You then try to remember another behavioral pattern, which is the Observer Pattern. You also feel like you've seen this code base before.

Observer Pattern is when you want to represent a one-to-many relationship, such as if one object is modified then its dependent objects will be notified immediately. When the state of the object being observed changes, that information is sent to the observers of said object.

One way to visualize the Observer Pattern is with the flow of a newspaper subscription. The newspaper publisher is the **SUBJECT** and the subscribers are the **OBSERVERS**. When the publisher releases a newspaper, the subscribers receive that information.

## Observer Pattern: The Task

There are blank parts of the code that you need to finish.

The implementation you want to do is whenever you add a new `MagicResearch` to the `MagicAssociation`, the `Researchers` are automatically updated with that new research in their research list.

There are three types of `MagicResearch` you need to implement in the code: **ArcaneMagic**, **ElementalMagic** and **UniversalMagic**. You must also make sure the following points are fulfilled:

- The `Arcanist` can only research **ArcaneMagic** and **UniversalMagic**.
- The `Elementalist` can only research **ElementalMagic** and **UniversalMagic**.
- The `GrandMagus` can research ALL types of magic.

Checklist :

- [ ] Read the tutorial
- [ ] Do Observer problem set
- [ ] Do Strategy problem set
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTg3MjcwNjgzMiw2OTMyMzYwODcsLTcyMD
E0Nzg4MywtMTUyMDM5ODg0OSw5MTQ1MTU0NTMsLTE2MjczMDAx
OTcsLTE2MzIzOTg0OTAsLTQwNjE5ODQ0NywxMDIxODk0ODcyLD
E4MjgxNzc5MjQsLTM2ODg3NTc4MCwtMTU2MjcwNTMwNyw0MDky
ODQ4MzksNDgxNzI1OTEyLDEwODk4ODA0ODUsMTAzMTE4NDUzOF
19
-->