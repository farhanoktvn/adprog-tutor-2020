package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public interface DefenseAction extends Action {
	
	String defense();
	
}