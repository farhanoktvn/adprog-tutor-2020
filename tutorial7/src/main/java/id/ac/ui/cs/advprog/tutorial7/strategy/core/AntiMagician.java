package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class AntiMagician extends Magician {
	
	// TO DO : Equip this class with actions:
	// AttackAction : MagicMissile
	// DefenseAction : Counter
	// SupportAction : Regenerate
	// Hint : Finish completing Magician class constructor first
	public AntiMagician(String name){
		super(name);
	}
}