package id.ac.ui.cs.advprog.tutorial8.decorator.service;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.Skill;

public interface EnhanceService {

    public void enhancePower();
    public void enhanceManacost();
    public void enhanceFreeze();
    public void enhanceLifesteal();
    public Iterable<Skill> getAllSkills();
}
