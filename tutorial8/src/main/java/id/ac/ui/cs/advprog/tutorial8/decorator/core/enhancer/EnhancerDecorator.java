package id.ac.ui.cs.advprog.tutorial8.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.Skill;

public enum EnhancerDecorator {
    POWER_UPGRADE,
    MANACOST_UPGRADE,
    FREEZE_UPGRADE,
    LIFESTEAL_UPGRADE;

    public Skill addSkillEnhancement(Skill skill){

        if (this == EnhancerDecorator.POWER_UPGRADE) {
            skill = new PowerUpgrade(skill);
        } else if (this == EnhancerDecorator.MANACOST_UPGRADE) {
            skill = new ManacostUpgrade(skill);
        } else if (this == EnhancerDecorator.FREEZE_UPGRADE) {
            skill = new FreezeUpgrade(skill);
        } else if (this == EnhancerDecorator.LIFESTEAL_UPGRADE) {
            skill = new LifestealUpgrade(skill);
        } else {
            //do nothing
        }

        return skill;

    }
}
