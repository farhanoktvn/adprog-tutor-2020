package id.ac.ui.cs.tutorial10.repository;

import id.ac.ui.cs.tutorial10.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserRepository extends JpaRepository<UserModel, String> {
    UserModel findByUserName(String username);
}
