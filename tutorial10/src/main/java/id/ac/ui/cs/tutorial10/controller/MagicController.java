package id.ac.ui.cs.tutorial10.controller;

import id.ac.ui.cs.tutorial10.model.MagicModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import id.ac.ui.cs.tutorial10.service.MagicService;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/magic")
public class MagicController {

    @Autowired
    MagicService magicService;

    @GetMapping("/")
    public Flux<MagicModel> getAllMagic() {
        //ToDo: Implement this.
        return null;
    }

    @PostMapping("/create/")
    public ResponseEntity createMagic(@RequestBody MagicModel magicModel) {
        //ToDo: Implement this.

        return ResponseEntity.ok("Added new Magic");
    }

}   