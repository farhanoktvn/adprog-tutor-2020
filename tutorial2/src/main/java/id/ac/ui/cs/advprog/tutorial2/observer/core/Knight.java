package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Knight extends Adventurer {

    private String name;

    public Knight(Guild guild) {
        super(guild);
        this.name = "Knight";
    }

    @Override
    public void update() {
        this.getQuests().add(this.guild.getQuest());
    }

    @Override
    public String getName() {
        return this.name;
    }
}
