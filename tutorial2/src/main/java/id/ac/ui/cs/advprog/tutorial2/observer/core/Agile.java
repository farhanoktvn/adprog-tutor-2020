package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Agile extends Adventurer {

    private String name;

    public Agile(Guild guild) {
        super(guild);
        this.name = "Agile";
    }

    @Override
    public void update() {
        if (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("R")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }

    @Override
    public String getName() {
        return this.name;
    }
}
