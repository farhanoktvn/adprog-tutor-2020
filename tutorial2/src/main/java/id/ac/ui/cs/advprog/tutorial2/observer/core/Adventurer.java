package id.ac.ui.cs.advprog.tutorial2.observer.core;

import java.util.ArrayList;
import java.util.List;

public abstract class Adventurer {
        protected Guild guild;
        private List<Quest> quests = new ArrayList<>();

        public Adventurer(Guild guild) {
                this.guild = guild;
                this.guild.add(this);

        }

        public List<Quest> getQuests() {
                return this.quests;
        }

        abstract void update();

        abstract String getName();
}
