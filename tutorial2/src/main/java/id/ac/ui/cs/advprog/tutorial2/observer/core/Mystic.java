package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Mystic extends Adventurer {

    private String name;

    public Mystic(Guild guild) {
        super(guild);
        this.name = "Mystic";
    }

    @Override
    public void update() {
        if (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("E")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }

    @Override
    public String getName() {
        return this.name;
    }
}
