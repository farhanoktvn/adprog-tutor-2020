package id.ac.ui.cs.tutorial5.core;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class CraftItem {
    private List<String> steps;
    private String name;

    public CraftItem(String name) {
        this.name = name;
        steps = new ArrayList<>();
    }

    public Craftable addRecipes(Craftable recipe) {
        steps.add(recipe.craft());
        return recipe;
    }

    public void composeRecipes() {
        steps.add( "Done crafting " + name);
    }

    public String getName() {
        return name;
    }

    public List<String> getSteps() {
        return steps;
    }
}
