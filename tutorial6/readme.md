# Tutorial 6 :  The Module System
You wake up to the sound of a distant thunder strike. It was still early in the morning, but you can see that it was visibly dark outside. The sounds of pouring rain decorate the atmosphere.  You look at the empty bed next to you as if expecting someone to be there, shake your head, and carry on with your morning duties.

On your way to the dining hall, you overheard a conversation in the nearby hallway. It was The Manager's voice, and a voice you've never heard before. You can't quite make out what the conversation was, but what you did hear was your name being mentioned by the voice you've never heard before. A chill goes down your spine. You quickly move along to the dining hall, making sure no one else notices you being there.

After breakfast, you return to your working desk. You wait for a little bit, wondering what the Manager was doing earlier. Just as you almost dozed off, the Manager calls you, holding several papers in his hand.

## Module System
The Manager shows you some research documents. The documents explain a form of magic that works using modules, but doesn't use the magic of Gradle's  automatic build. You vaguely remember a concept called Java Module System or JMS.

In Java, there is something known as the Module System. It is a technology that has been known since the older versions, and allows older code to be refactored into modules in newer versions. Normally you would be running programs in the classpath, but with module systems, you will be running code in the module path instead. The Recruiter says The Magic Association needs more information about this system, and asks you to work on some code that uses this module system. 

This time you make the code without SpringBoot, but with the command line as a basis. All the code does is insert a JSON string into a file and saves it. But the code implements the module system that you have just remembered about.

You finish the job, applying Module System in your code. You feel it's quite a good representation of what a Java Module System was like back in your world. After presenting your code to the Magic Association board, they request that you make a report that explains what you've made, and also some theories regarding the module system in question.

Your job now is to write an answer.md file consisting of the following (minimum 1 paragraph each, 1 paragraph consists of at least 3 sentences):

- What is Java Module System, and why use it?
- What's the difference between a modular system and a normal system (not using modules).
- The flow of the program, and how the module system works in the program.
- Create a dependency graph of the project with any tools you like (for instance, [draw.io](https://www.draw.io/)), and place the graph image in the markdown file. Explain the graph you made.

Checklist:

- [ ] Read the tutorial
- [ ] Write answer.md

