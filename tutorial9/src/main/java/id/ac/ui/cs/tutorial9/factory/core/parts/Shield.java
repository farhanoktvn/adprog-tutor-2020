package id.ac.ui.cs.tutorial9.factory.core.parts;

public class Shield implements Weapon {
    public String attack(){
        return "Shield : A weapon that can protect you from harm and also can deliver an attack. Yes, shield is a weapon";
    }
}