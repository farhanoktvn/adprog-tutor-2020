package id.ac.ui.cs.tutorial9.factory.core.parts;

public class SelfOfferingSacrifice implements Aura {
    public String activate(){
        return "Self Offering Sacrifice : An aura that allows the knight to protect surrounding allies";
    }
}