package id.ac.ui.cs.tutorial9.factory.core.magicknight;

// BONUS : This class is actually unnecessary.
// You might want to delete this class but you have to make sure the program still runs properly
public class Duellist extends MagicKnight {

    // TO DO : modify the constructor implementation to match the MagicKnight's constructor
    public Duellist(String name){
        super(name);
    }
}