package id.ac.ui.cs.tutorial9.factory.core.factory;

import id.ac.ui.cs.tutorial9.factory.core.parts.Affinity;
import id.ac.ui.cs.tutorial9.factory.core.parts.Aura;
import id.ac.ui.cs.tutorial9.factory.core.parts.Spell;
import id.ac.ui.cs.tutorial9.factory.core.parts.Weapon;

// TO DO : Complete the implementation of this factory
// Affinity : ThunderCrossSplitAttack
// Aura : ThunderCrossSplitAttack
// Spell : ThunderCrossSplitAttack
// Weapon : ThunderCrossSplitAttack
public class DireFactory implements KnightFactory {
    public Affinity createAffinity(){
        return null;
    }

    public Aura createAura(){
        return null;
    }

    public Spell createSpell(){
        return null;
    }

    public Weapon createWeapon(){
        return null;
    }
}