package id.ac.ui.cs.tutorial9.factory.core.parts;

public class Staff implements Weapon {
    public String attack(){
        return "Staff : A weapon that stores very high magical energy";
    }
}