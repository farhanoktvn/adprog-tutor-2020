package id.ac.ui.cs.tutorial9.factory.controller;

import id.ac.ui.cs.tutorial9.factory.service.KnightService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class FactoryController {
    @Autowired
    private KnightService knightService;
    
    @GetMapping("/knights")
    public String getKnights(Model model){
        model.addAttribute("magicians", knightService.getKnights());
        return "factory/knights";
    }

    @PostMapping("/newKnight")
    public String createKnight(HttpServletRequest request){
        String name = request.getParameter("name");
        String type = request.getParameter("type");
        knightService.createKnight(name, type);
        return "redirect:/knights";
    }
}