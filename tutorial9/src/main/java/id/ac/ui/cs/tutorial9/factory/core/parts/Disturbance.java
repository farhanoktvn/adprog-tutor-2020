package id.ac.ui.cs.tutorial9.factory.core.parts;

public class Disturbance implements Affinity {
    public String getDescription(){
        return "Disturbance : This knight utilizes disturbance to let them manipulate their enemy";
    }
}