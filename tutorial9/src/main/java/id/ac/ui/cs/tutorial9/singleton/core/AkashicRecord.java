package id.ac.ui.cs.tutorial9.singleton.core;

import java.util.ArrayList;
import java.util.Random;

public class AkashicRecord {

    private ArrayList<String> records = new ArrayList<>();
    private Random randomizer = new Random();

    //ToDo: Complete me with any Singleton approach

    public String getRandomRecord() {
        if (records.size() == 0) return "Akashic Record is empty";
        else return this.records.get(randomizer.nextInt(records.size()));
    }

    public void addRecord(String record) {
        this.records.add(record);
    }

}