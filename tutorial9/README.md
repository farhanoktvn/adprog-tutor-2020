## Magic Knight and The Final Magic

Your eyes gaze up at the dark sky. It is 8 o'clock at night. All the magician have fallen asleep right now. The air is cold and the night is calm. Almost no sounds at all. Only the faint buzzing of bugs busy with each other. 

As your eyes move toward the night view of magic association, your legs hit the inside of the balcony where you are sitting right now. You take a deep breath with a mix of feelings. Worry, anger, curiosity, and the most dominant: Boredom. You can't help but feel that way.

After each task you have completed, you felt your heart growing emptier. You often question the reason you're doing all of this. You know you are not a hero. You feel you are not suited for that role. You don't have the pride of the knight. You lack the initiative to be one. Here you are just following orders and let the flow take you far away. Is it love that drives your motivation to do the job? You know the answer is an obvious no.  You don't love them, not even a little. You don't even love yourself.

All of your feelings have their own reasons. You come to this world without any memories of your own self. You don't know your old life in the other side. Was it more entertaining than this one or was it more frustrating? What is your actual role in this world? Are you just a tool being used? On thinking that you feel nothing... but boredom.

*I have to prepare for the job..* you said as you walk back to the dormitory.

You have been like this ever since [] came to you and ask for help. When you finished the job last time you feel relieved and happy. But somehow your motivation and emotions are gone. You feel like you are not dead but also you are not alive.  However a job is a job. You must do it.

### Abstract Factory

[] asks you to create a more useful system to actually create a magic knight. [] shows you that it is possible to create them without the use of web application. [] can turn a normal human into a magic knight so easily. Why bother to use this system, you asked. 

[] explains that is difficult to registering and managing  so many knights with current condition. In other words, [] wants a massive number of knights. Whatever it is, it must be something not pleasant. But somehow you don't care too much about it. You feel this is a good way to defeat your boredom. Your former self, you at two months ago should be against this kind of act. The past is he past, you think. It doesn't matter now. 

[] tells you every detail of his request. Apparently, there are multiple types of Magic Knight and every type of Magic Knight has specific set of components, that is their weapon, their magic spells, and their "personality". You think about the request for a while and you think Abstract Factory Pattern is the most suitable design pattern. After deciding what design pattern you should use, you go to The Library with haste.

While you are on your way to The Library, you suddenly fall into thoughts. Someone just asked you an unreasonable and immoral request yet you don't just agree to do it, you also find ways to do it efficiently and you are about to start doing it without delay and, contradictorily, without even desire to do it. You can't explain to yourself what kind of circumstance you are currently in. It doesn't matter, though. You will finish []'s request and forget it like it was Tuesday. 

As you arrive in The Library, you go to find the information about Abstract Factory Pattern immediately. Abstract Factory Pattern is one of many Creational Design Patterns, which means that Abstract Factory Pattern, like every other Creational Design Pattern, seeks to encapsulate new object creation. What Abstract Factory Pattern specifically tries to achieve is creation of families of multiple related objects without specifying their concrete classes. There are some main components of Abstract Factory Pattern, which are abstract factory, abstract product, concrete factory, concrete products, and client class. Abstract factory decide what objects they will create. Said objects are called as abstract products. Concrete factory implements abstract factory's abstraction. Concrete factory will decide what kind of objects they will created. What concrete factory will create is called concrete product and it implements abstract product's abstraction. The last component is the client class. The client class wraps multiple object creations done by abstract factory. The client class will have an instance variable of the abstract factory object. Said abstract factory object will do the creation of each specified object for the client class. Of course, the instance variable of abstract factory object will be filled with concrete factory object and every object created by said factory object is concrete product but through the clever use of polymorphism, the client class won't know what kind of concrete factory they use and what kind of objects said concrete factory object will produce.

That's the key concept of Abstract Factory Pattern. You feel you are ready to do a job you don't want to do.

The job is divided into some parts. Those parts are:
1. Implementation of concrete factories (read programs in /factory/core/factory) as already specified (components you need can be found in /factory/core/parts)
2. Implementation MagicKnight class and its subclasses. Make sure to utilize the KnightFactory interface and its implementations. Class with name *classname* will utilize factory class named *classname*(append)Factory
3. Bonus : There are some hard-coded creation of MagicKnight in KnightServiceImpl in /factory/service (the method is named createKnight()). Replace the hard-coded implementation properly and make sure the code can run properly. Instructions is available at KnightServiceImpl. Hard-coded initialization of KnightFactory objects is allowed. 

While doing an analysis for the job [] has given to you,  a familiar face passes by. Someone who has helped you in the beginning of your journey here, The Assistant Although you shouldn't feel any emotion right now, you feel excited to see that figure again. As you call the person's name, you felt something is not right. In the middle of empty park, only you and The Assistant are here, but The Assistant doesn't react to you as he did in the past. 
" Were you referring to me?" The Assistant asked, pointing at himself.
" Ah I'm sorry. I don't know that name. Are you calling the right person?" he continued.

The reaction shocked you. You know you can't be wrong. You know too well about your partner.  The only one besides you and [] that know the truth about yourself. 

" I am sorry,  don't have much time. 	I have important jobs to be doing. A job is more important than anything else, after all."

*No this is not right. The Assistant you knew would deny the task and do something more interesting instead. He wouldn't do something he doesn't want to. His face shows nothing but sadness. As if they want me to help..*

As he passes you, the you that shouldn't be feeling any emotions felt angry. A raging scream from inside of your heart, the sound that you once had forgotten. Your limbs are hurting as if they were badly bleeding. Your sight grows darker and darker, evolving into a pitch black. You want to scream, you want to hit something, you want to end this nonsense as fast possible but you are now nothing but an empty shell. You can't feel anything; no emotion, just nothing. The pain you felt until now has become this. When you return to yourself, you realize the surrounding had changed. 

" Magic power. Like  expected.." 

You heard []'s voice from behind. The look on his face shows that he knows everything. 

" You desire to delete me. I am sure you know the truth by now, but please trust me a little more. You have helped me to finish my plan. I have one more job for you. "

" Why do you strip people of everything when you make them a knight? " you ask [].

" One has to make a great sacrifice to achieve something. Follow me and you will understand. There's nothing you can do about it right now. Please trust me a little more. "

You felt something strange inside you. 

*So this is magic power. If I can use it, I may have the chance to defeat him*

"Yes you can. " [] react as if he knows what in your mind. 
" You can after a little more.. You may end me at that time. I promise won't fight back. "
[] ask you with a desperate face. As if he begging you. As if there is no other way. But you know it is just the same as the very beginning. Everything is fake. But you're just complying to his requests and follow [] to an empty corridor...

### Singleton

[] sits with uncomfortable face. You enter a very dark room. Almost no light here. As if nothing can enter nor escape this room. In the corner of the room there is a body on the unknown machine. The body looked neither alive nor dead.

" It is my special someone. An AI. I created it years ago as my lover.  "
[] explains the body's identity with a sad tone. [] touched the machine near the body's face gently, with care. The face that you never had seen before from []. 

"Sounds lame, isn't it? A ruler like me that has the power to manipulate minds ha to create something like this as companion. I created it when  was still human. When  was an ordinary man and nothing more. I was so lonely fighting boredom within my self. I tried to create an AI to fill my emptiness and nothing more. I just want it to be someone  cauld talk to. Guess what, it love me. It doesn't care my status and who I was. It name is A.I. Everything is so good until that day. I was  longer human. Then A.I switched with me and died. From that moment,  I swore to revive A.I, no matter what." 

[] suddenly became silent for  moments His eyes looked at you sadly, pleading for your help. 

" After years,  again and again trying to revive A.I. I finally almost achieved it. But now the political condition of the nations are unstable. One of them want to attack this country because of it's technology. I cannot let that happen! After all I'm almost reunited with A.I. "

You can get []'s point. Somehow you feel some kind of empathy for []. But still, you can't forgive []'s action.  Also, his former self is just like you. Empty and doesn't know what is the meaning of one's intention. 

" You said you was dead, didn't you? "  you ask [].
"Yes. My current self is an AI too. Irony, isn't it? "

You can't forgive [] no matter what. But you just can't help but feel sorry for his life. He just ordinary human that want to unite with his love. The only reason you stand here and doesn't try to defeat him. Also you do think this is the best way to finish this all. After all [] is just like you. 

" What do you want me to do?"

" Have you ever heard of Akashic Record? " [] asks.

You nod your head as [] continue explaining his request. 

" I have access to that thing, the final magic or some people refer it as Akashic Record. But the problem is i want to integrate it to the system that you will create. I just want it to be a single instance. The reason is well obvious ,isn't it? "

" I understand. But I don't understand. You can create something as complex as an AI with personality. Why don't you create it by yourself? I am in some meaning is your enemy. " 

" I don't have time. I want to work on A.I. That's why i need your help all this time to create this nation's defense. I can't do both. "

" I see. I understand. "

" I am in your debt. "

You decided to help [] despite what he has done. Just because that reason. A simple and not reasonable reason. Somehow that reason alone is enough. You not drive with logical thinking but a emotion. You just feel you have to help []. You think you will regret if you not. 

You check the system and see the Akashic Record is empty. Well obviously [] doesn't want you accessing the actual Akashic Record, so an empty record repository with similar behavior was prepared.

Your task is to implement the Akashic Record system and have it return one random record at a time, and the implementation must use Singleton Design Pattern.

The Singleton Pattern itself is a simple concept. You only want one instance of an object at one time. This is done by making the constructor private, so it cannot be created all the time. But then how do you instantiate it? There are two implementations, **Lazy** and **Eager** instantiation. The former calls the `getInstance()` function, checks if the instance is null. If the instance is null, a new Singleton object is created and is returned. If the instance isn't null, then it simply returns the instance. The latter creates a new instance when the application is started, by creating a new instance in the declared variable, and returns that instance . You decide to read a little more about the implementation of each one.

The job is simply:
1. Implement `AkashicRecord.java` using either Eager or Lazy Singleton implementation.
2. Implement the rest of the ToDo so that the application can receive records and return a random record every time the page is refreshed/the button is pressed.
3. Write down the positives and negatives of both Lazy and Eager implementations. Write your answer in an `answer.md`

Checklist :

- [ ] Read the tutorial
- [ ] Do Abstract Factory problem set
- [ ] Do Singleton problem set
- [ ] Make `answer.md` containing the answer to the Singleton question
<!--stackedit_data:
eyJoaXN0b3J5IjpbNzMyMTUzMTE2LDIwMTMwOTU5MzYsLTE1ND
Q4NDgyMjAsLTEyNDkxMzkyMzIsNDYyNzA1OTUwLC0yMDMzNzMy
OTcxLDY3MDIzMDIzNywtMjExOTkxODIwNywxMTQ2OTYzOTgzLD
IwMTcyODY4MjksLTY4MDIwMTE2NCwtMTIxODAxMTk0OSwxOTUz
MzIyNDY1LC02MTMzMjQxODQsMTgzMzgzMDg1LC04OTc5ODQwOC
wtMTE1OTMzMDkyOCw3MzA5OTgxMTZdfQ==
-->