# Answer to Tutorial-3:
#### 1. Program Flow
1. Open the program on localhost:8080/run_manalith on browser
2. Pick the desired synthesis type and click 'Add new Synthesis'
3. Fill in the form and click submit.
4. The controller will make a synthesis object based on the type.
5. The added synthesis will then be added to synthesis repository.
6. When we click the process request button, request will be made based on the synthesis repository.
7. The request will be executed according to the queue made by the action in the previous step.
8. The status message will then be collected in variable statusList.
9. The statusList is showed to the user.
#### 2. Race Condition Identification
The part of program that caused the race condition is the 'startThread' method located in ManalithServiceImpl.java'. This method is causing race condition because the method run 'runManalith' but it is also return to 'runManalith' method (line 43-44 | ManalithServiceImpl.java). Therefore there are two 'runManalith' method running at the same time.
#### 3. Why race condition happen?
Race condition happen because multiple processes are using the same resource(s) at the same time. In this case both runManalith processes are running the Manalith.java processRequest method which is using the Integer variable 'mana'.
#### 4. Solution to the problem
One of the solution of the problem is to pick which method that would be run on ManalithServiceImpl.java (stardThread or runManalith). Then if we use multi-threading, we need to lock the variable used by the thread so that other thread can not access nor edit the variable that is being used. We can also change the data structure queue into a data structure specified in java.util.concurrent.


