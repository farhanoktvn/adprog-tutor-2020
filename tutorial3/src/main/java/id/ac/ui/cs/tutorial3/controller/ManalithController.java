package id.ac.ui.cs.tutorial3.controller;

import id.ac.ui.cs.tutorial3.core.Synthesis;
import id.ac.ui.cs.tutorial3.core.SynthesisCatalyst;
import id.ac.ui.cs.tutorial3.core.SynthesisKnight;
import id.ac.ui.cs.tutorial3.service.ManalithService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ManalithController {

    @Autowired
    private ManalithService manalithService;


    @GetMapping("/run_manalith")
    public String run_manalith(Model model) {
        model.addAttribute("synthesis", manalithService.getSynthesises());
        return "manalith";
    }

    @GetMapping("/add_synthesis")
    public String add_synthesis_Page(Model model, HttpServletRequest request) {
        String type = request.getParameter("type");
        System.out.println(type);
        Synthesis synthesis;
        if (type.equals("knight")) {
            synthesis =  new SynthesisKnight("", 0);
        } else  {
            synthesis =  new SynthesisCatalyst("", 0);
        }
        model.addAttribute("synthesisz",synthesis);
        return "add_synthesis";
    }

    @PostMapping("/add_new_knight")
    public ModelAndView add_new_synthesis_knight(@ModelAttribute SynthesisKnight synthesis) {
        manalithService.addSynth(synthesis);
        return new ModelAndView("redirect:/run_manalith");
    }

    @PostMapping("/add_new_catalyst")
    public ModelAndView add_new_synthesis_catalyst(@ModelAttribute SynthesisCatalyst synthesisCatalyst) {
        manalithService.addSynth(synthesisCatalyst);
        return new ModelAndView("redirect:/run_manalith");
    }

    @PostMapping("/process_request")
    public String process_request(Model model) {
        model.addAttribute("synthesis", manalithService.getSynthesises());
        model.addAttribute("status_req", manalithService.processRequest());
        return "manalith";
    }

}
