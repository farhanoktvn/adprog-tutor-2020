1. Explain what is contained in the /actuator endpoint.

    The /actuator endpoint contains all the resources that can be accessed to monitor the usage and processes of our application.
    ![/actuator](images/actuator-1.JPG)

2. Try and display some data in the /actuator/info endpoint. Any data is fine.
    
    To display some info to /actuator/info, I add some info to application.properties.
    ```
    ## Configuring info endpoint
    info.app.name=Spring Tutorial 4
    info.app.description=Exercise to display info with actuator
    info.app.version=1.0.0
    ```
   
    The resulting display of the code is as follows:
    ![/actuator/info](images/actuator-info.JPG)

3. Try the /actuator/prometheus endpoint. Look at its contents and write what you understand about it.
    
    The content of /actuator/prometheus is as follows:
    ![/actuator/prometheus](images/actuator-prometheus.JPG)
    
    Based on the output of /actuator/prometheus, I think that it consists of data that is used by prometheus to perform monitoring and data logging. For example, it shows the state of threads and cpu usage.
    
4. Change your prometheus.yml file so that it targets the Prometheus metrics endpoint at localhost:8080.
    
    Inside the prometheus.yml file, I add the following lines of codes:
    ```
    - job_name: 'springboot'
      scrape_interval: 5s
      metrics_path: '/actuator/prometheus'
      static_configs:
      - targets: ['localhost:8080']
    ```
   
5. Try adding a Timed annotation to a function, check it with Prometheus, and explain what you see.
    
    I tried to add Timed annotation to the function customErrorPage()
    ```
   @Timed("goof")
   @GetMapping("/goof")
   @PreAuthorize("isAuthenticated()")
   public String customErrorPage(Model model) {
       return "error/custom_error";
   }
    ```
   For this example, I chose to execute goof_seconds_count.
   ![goof](images/prometheus-goof.JPG)
   
   In this picture, we can see that /goof has been requested three times according to the Y-axis and we can also see the difference between the time when the request received and the request sent.
    
6. Set-up Grafana and explain what you did.
    
    To set up Grafana, the first thing we have to do is to create a datasource from Prometheus. Then, we can build/import a dashboard with JVM configuration. 
    ![grafana-dashboard](images/grafana-dashboard.JPG)